## 1. Implementation

### 1.1. Language

Python 3.6.

### 1.2. Validation logic

The [official validation pattern](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/488478/Bulk_Data_Transfer_-_additional_validation_valid_from_12_November_2015.pdf) docs used for validating input postal codes) searches for matches.
However, the pattern doesn't cover 100% of cases, so
[postcodes.io service](https://postcodes.io/) additionally requrested, if the initial pattern finds the code valid.


## 2. Interface

CodesFormatter class is responsible for formatting and validating keys.

### 2.1. Validation

CodesFormatter.is_valid_code(code) class-method accepts a code string
and returns a boolean value indicating, if the input code is valid:

```python
is_valid = CodesFormatter.is_valid_code('EC1A 1BB')
print(is_valid)
# True
```


### 2.1. Formatting

CodesFormatter.format_code(code) class-method accepts a code string
and returns a formatted code string:

```python
code = CodesFormatter.format_code('ec1a1bb')
print(code)
# EC1A 1BB
```


## 3. Unit tests

Running unit tests:
```
$ cd postcodes
# "python3 -m unittest" command required to handle imports properly.
$ python3 -m unittest tests/unit/test_codes_formatter.py 

# ........
# ----------------------------------------------------------------------
# Ran 8 tests in 0.001s
```

### 4. Dependencies

The lib uses python-requests. 
To install the requirements run:
```
pip3 install -r requirements.txt
```

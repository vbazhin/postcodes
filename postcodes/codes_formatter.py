"""Format and validate postcodes."""
import re
import json
from .errors import InvalidPostalCode
import requests


class CodesFormatter:
    """The class provides an interface to validate UK postal codes.

    The validation and formatting methods are static,
    i.e. the class is used as a namespace.

    The validation RegExp pattern provided by the Govermental documentation:
        https://assets.publishing.service.gov.uk/government/uploads/
        system/uploads/attachment_data/file/488478/Bulk_Data_Transfer
        _-_additional_validation_valid_from_12_November_2015.pdf
    """

    SERVICE_BASE_URL = 'http://api.postcodes.io/postcodes'
    VALIDATION_ENDPOINT = 'validate'

    pattern = re.compile('^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|'
                         '(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))[0-9][A-Za-z]{2})$')

    @classmethod
    def is_valid_code(cls, code):
        """Validate postal code.

        The code validated online only if the official
        regexp doesn't mark the code as invalid.

        :param code: Input code string.
        :type code: str.

        :return: Is the code valid.
        :rtype: bool.
        """
        code = cls._remove_spaces(code)
        return bool(cls.pattern.match(code)) and cls._validate_online(code)

    @classmethod
    def _validate_online(cls, code):
        """Request UK Postcodes validation service.

        :param code: Input code string.
        :type code: str.

        :return: Is code valid.
        :rtype: bool.
        """
        res = requests.get('{url}/{code}/{endpoint}'.format(
            url=cls.SERVICE_BASE_URL,
            code=code,
            endpoint=cls.VALIDATION_ENDPOINT
        ))
        res.raise_for_status()
        is_valid = json.loads(res.text)['result']
        return is_valid

    @classmethod
    def format_code(cls, code):
        """Format postal code.

        According to the documentation, all letters must be uppercase,
        and ourward code must be separated from inward code with a space.

        :param code: Input code string.
        :type code: str.

        :return: Formatted code.
        :rtype: str.
        """
        if not cls.is_valid_code(code):
            raise InvalidPostalCode(
                'Can\'t format the invalid '
                'postal code: {}'.format(code)
            )
        code = cls._format_case(code)
        code = cls._format_spaces(code)
        return code

    @classmethod
    def _format_case(cls, code):
        """Convert letters to upper case.

        :param code: Postal code string.
        :type code: str.

        :return: Formatted code.
        :rtype: str.
        """
        code = code.upper()
        return code

    @classmethod
    def _format_spaces(cls, code):
        """Format spaces in code string.

        The outward code must be separated from inward with a space.
        The inward pattern always contains 3 chars (digit + 2 letters),
        it's sufficient to add a space prior to 3rd to the last char
        of the code string after removing all spaces from the input string.

        :param code: Postal code string.
        :type code: str.

        :return: Formatted code.
        :rtype: str.
        """
        code = cls._remove_spaces(code)
        code = '{outward} {inward}'.format(
            outward=code[:-3],
            inward=code[-3:]
        )
        return code

    @staticmethod
    def _remove_spaces(code):
        """Remove spaces from input string

        :param code: Postal code string.
        :type code: str.

        :return: Formatted code.
        :rtype: str.
        """
        return code.replace(' ', '')

"""Exception classes for CodesFormatter."""

class CodesFormatterException(BaseException):
    ...


class InvalidPostalCode(CodesFormatterException):
    ...
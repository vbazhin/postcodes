"""ThreeFive program unit tests module."""
import unittest
from postcodes import CodesFormatter
from postcodes.errors import InvalidPostalCode


#TODO: Add much more cases to test.


class TestCodesFormatter(unittest.TestCase):
    """Test ThreeFive module functions."""

    def test_is_valid_code(self):
        self.assertTrue(CodesFormatter.is_valid_code('EC1A 1BB'))
        self.assertTrue(CodesFormatter.is_valid_code('M1 1AE'))
        self.assertTrue(CodesFormatter.is_valid_code('B33 8TH'))
        self.assertTrue(CodesFormatter.is_valid_code('CR2 6XH'))
        self.assertTrue(CodesFormatter.is_valid_code('DN55 1PT'))

    def test_is_valid_code_case_insensitive(self):
        self.assertTrue(CodesFormatter.is_valid_code('ec1a 1bb'))
        self.assertTrue(CodesFormatter.is_valid_code('ec1a 1BB'))
        self.assertTrue(CodesFormatter.is_valid_code('ec1a 1BB'))
        self.assertTrue(CodesFormatter.is_valid_code('EC1a 1bB'))
        self.assertTrue(CodesFormatter.is_valid_code('EC1a 1bb'))

    def test_is_valid_misformatted_spaces(self):
        self.assertTrue(CodesFormatter.is_valid_code('EC1A1BB'))
        self.assertTrue(CodesFormatter.is_valid_code('EC1A 1BB'))
        self.assertTrue(CodesFormatter.is_valid_code('EC1A  1BB'))
        self.assertTrue(CodesFormatter.is_valid_code('  EC1A1BB'))
        self.assertTrue(CodesFormatter.is_valid_code('  EC1A1BB  '))
        self.assertTrue(CodesFormatter.is_valid_code(' E C 1 A 1 B B '))

    def test_is_valid_code_invalid_code(self):
        self.assertFalse(CodesFormatter.is_valid_code('EC1A 1BB!'))
        self.assertFalse(CodesFormatter.is_valid_code('AAA1A 1AA'))
        self.assertFalse(CodesFormatter.is_valid_code('AAA1 1AA'))
        self.assertFalse(CodesFormatter.is_valid_code('AAA1 1A'))
        self.assertFalse(CodesFormatter.is_valid_code('AAA1 1AA'))
        self.assertFalse(CodesFormatter.is_valid_code('AAAA 1AA'))
        self.assertFalse(CodesFormatter.is_valid_code('A1AA 1AA'))
        self.assertFalse(CodesFormatter.is_valid_code('1A AAA'))
        self.assertFalse(CodesFormatter.is_valid_code('11A AAA'))
        self.assertFalse(CodesFormatter.is_valid_code('AB1 1AA'))
        # TODO: Add more negative cases.

    def test_format_code_proper_format(self):
        self.assertEqual(CodesFormatter.format_code('EC1A 1BB'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('M1 1AE'), 'M1 1AE')
        self.assertEqual(CodesFormatter.format_code('B33 8TH'), 'B33 8TH')
        self.assertEqual(CodesFormatter.format_code('CR2 6XH'), 'CR2 6XH')
        self.assertEqual(CodesFormatter.format_code('DN55 1PT'), 'DN55 1PT')

    def test_format_code_misformated_spaces(self):
        self.assertEqual(CodesFormatter.format_code('EC1A1BB'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('EC1A  1BB'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('  EC1A1BB'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('EC1A 1BB  '), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('  EC1A 1BB  '), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('E C 1 A 1 B B'), 'EC1A 1BB')

    def test_format_code_lower_case(self):
        self.assertEqual(CodesFormatter.format_code('EC1A 1BB'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('ec1A 1bb'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('ec1a 1BB'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('EC1A 1bB'), 'EC1A 1BB')
        self.assertEqual(CodesFormatter.format_code('EC1A 1bb'), 'EC1A 1BB')

    def test_format_code_invalid_format(self):
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, 'AA1A 1AA!')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, '1AA1A 1AA')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, 'AAA1 1AA')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, 'AAA1 1A')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, 'AAA1 1AA')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, 'AAAA 1AA')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, 'A1AA 1AA')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, '1A AAA')
        self.assertRaises(InvalidPostalCode, CodesFormatter.format_code, '11A AAA')
        # TODO: Add more negative cases.


if __name__ == '__main__':
    unittest.main()
